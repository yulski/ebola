set terminal wxt size 450,270 enhanced font 'Verdana,10' persist
set style line 1 linecolor rgb '#8ae600' linetype 1 linewidth 2
set style line 2 linecolor rgb '#ffff3c' linetype 1 linewidth 2
set style line 3 linecolor rgb '#d5481e' linetype 1 linewidth 2
set style line 4 linecolor rgb '#000000' linetype 1 linewidth 2
set xlabel "generations"
set ylabel "num cells"
plot 'ebola-res.dat' using 2:xticlabels(1) t 'hea' with lines ls 1, '' using 3 t 'inf' with lines ls 2, '' using 4 t 'con' with lines ls 3, '' using 5 t 'dea' with lines ls 4

