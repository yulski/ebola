#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define CORNER_CELL 0
#define EDGE_CELL 1
#define NORMAL_CELL 2

#define MIN_INFCH 40
#define MAX_INFCH 90

#define HEALTHY 0
#define INFECTED 1
#define CONTAGIOUS 2
#define DEAD 3
#define C_LATENCY 15
#define D_LATENCY 30

typedef struct {
	int x;
	int y;
	int s;
	int p;
} cell;

int dim = 500;
cell grid[500][500];
int gens = 365;
int iinfc = 5;

void grid_init();
void loop();
void run_gen();
void find_neighbors(int i, int j, cell *n);
int get_cell_pos(int i, int j);
int count_neighbors(int i, int j);
void inf_init();
void print_stats();
void wres(int g);
int calc_infch(cell *n, int nnum);
void infect(int i, int j);
void make_contagious(int i, int j);
void cure(int i, int j);
void kill(int i, int j);
int r_int(int min, int max);
void finit();
void wgrid(int g);
void print_grid(char *out);

int totalh = 250000;
int totali = 0;
int totald = 0;
int totalc = 0;

int winterval = 100;

char *res_fname = "./res/ebola-res.dat";

int fgc = 0;

int main(int argc, char **argv) {
	/* initialize the grid and then start running generations */
	if(argc > 2) {
		gens = atoi(argv[1]);
		winterval = atoi(argv[2]);
	}
	srand((unsigned) time(NULL));
	puts("simulation running...");
	grid_init();
	inf_init();
	finit();
	wres(0);
	wgrid(0);
	loop();
	wres(gens);
	wgrid(gens);
	print_stats();
	return 0;
}

void grid_init() {
	int i, j;
	/* set each cell to 0 */
	for(i=0; i<dim; i++) {
		for(j=0; j<dim; j++) {
			grid[i][j].s = HEALTHY;
			grid[i][j].p = 0;
		}
	}
}

void loop() {
	/* call run_gen once per generation */
	int i;
	for(i=0; i<gens; i++) {
		run_gen();
		if(i > 0 && i % winterval == 0) {
			wres(i);
			wgrid(i);
		}
	}
}

void run_gen() {
	/* run a single generation of the simulation */
	int i, j, k, nnum, r, inf = 0, infch = 0, dch=5, hch=5, min=1, max=100;
	cell cn;
	for(i=0; i<dim; i++) {
		for(j=0; j<dim; j++) {
			infch = 0;
			dch = 5;
			grid[i][j].p++;
			nnum = count_neighbors(i, j);
			cell n[nnum];
			find_neighbors(i, j, n);
			infch = calc_infch(n, nnum);
			r = r_int(min, max);
			if(r < infch && grid[i][j].s == HEALTHY) {
				infect(i, j);
			} else if(grid[i][j].s == INFECTED && grid[i][j].p >= C_LATENCY) {
				make_contagious(i, j);
			} else if(grid[i][j].s == CONTAGIOUS) {
				int p = grid[i][j].p >= D_LATENCY ? D_LATENCY : grid[i][j].p;
				dch += 2*p;
				r = r_int(min, max);
				if(r < hch) {
					cure(i, j);
				} else if(r < dch) {
					kill(i, j);
				}
			}
		}
	}
}

void find_neighbors(int i, int j, cell *n) {
	/* find and return the neighbor cells of a cell */
	int x[] = {i-1, i-1, i-1, i, i, i+1, i+1, i+1};
	int y[] = {j-1, j, j+1, j-1, j+1, j-1, j, j+1};
	int k, c=0;
	for(k=0; k<8; k++) {
		if(x[k] > -1 && x[k] < dim && y[k] > -1 && y[k] < dim) {
			n[c].x = x[k];
			n[c].y = y[k];
			c++;
		}
	}
}
	
int count_neighbors(int i, int j) {
	/* based on its position, return how many
		neighbors a cell has */
	int pos = get_cell_pos(i, j);
	if(pos == CORNER_CELL) {
		return 3;
	} else if(pos == EDGE_CELL) {
		return 5;
	} else {
		return 8;
	}
}

int get_cell_pos(int i, int j) {
	int max_dim = dim - 1;
	/* figure out of cell is in a corner, 
		on an edge (top, bottom, left, right), or sorrounded
		by other cells */
	if(i == 0) {
		if(j == 0 || j == max_dim) {
			return CORNER_CELL;
		} else {
			return EDGE_CELL;
		}
	} 
	else if(i == max_dim) {
		if(j == 0 || j == max_dim) {
			return CORNER_CELL;
		} else {
			return EDGE_CELL;
		}
	} 
	else if(j == 0 || j == max_dim) {
		return EDGE_CELL;
	} else {
		return NORMAL_CELL;
	}
}

void inf_init() {
	/* starts the infection */
	int min = 0, max = dim-1;
	double x;
	int i, j, c;
	for(c=0; c<iinfc; c++) {
		i = r_int(min, max);
		j = r_int(min, max);
		if(grid[i][j].s != INFECTED) {
			grid[i][j].s = INFECTED;
			grid[i][j].p = 0;
		} else {
			c--;
		}
	}

}

void print_stats() {
	printf("\ngenerations: %d\n", gens);
	printf("total healthy: %d\n", totalh);
	printf("total infected: %d\n", totali);
	printf("total contagious: %d\n", totalc);
	printf("total dead: %d\n", totald);
}

void wres(int g) {
	char res[100];
	sprintf(res, "%d\t%d\t%d\t%d\t%d\n", g, totalh, totali, totalc, totald); 
	FILE *fp = fopen(res_fname, "a");
	fputs(res, fp);
	fclose(fp);
}

int calc_infch(cell *n, int nnum) {
	int i, infch = 0;
	cell cn;
	for(i=0; i<nnum; i++) {
		cn = grid[n[i].x][n[i].y];
		if(cn.s == CONTAGIOUS) {
			if(infch == 0) {
				infch = MIN_INFCH;
			} else {
				infch += (int) (((float) MAX_INFCH / (float) nnum) - (float) MIN_INFCH);
			}
		}
	}
	return infch;
}

void infect(int i, int j) {
	grid[i][j].s = INFECTED;
	grid[i][j].p = 0;
	totalh--;
	totali++;
}

void make_contagious(int i, int j) {
	grid[i][j].s = CONTAGIOUS;
	grid[i][j].p = 0;
	totali--;
	totalc++;
}

void cure(int i, int j) {
	grid[i][j].s = HEALTHY;
	grid[i][j].p = 0;
	totalc--;
	totalh++;
}

void kill(int i, int j) {
	grid[i][j].s = DEAD;
	grid[i][j].p = 0;
	totalc--;
	totald++;
}

int r_int(int min, int max) {	
	double x;
	int r;
	x = (double) rand() / RAND_MAX;
	r = (max - min + 1) * x + min;
	return r;
}

void finit() {
	FILE *fp = fopen(res_fname, "w");
	fputs("# gen\thea\tinf\tcon\tdea\n", fp);
	fclose(fp);
}

void wgrid(int g) {
	char fname[50];
	sprintf(fname, "./res/ebola-grid-%d.dat", fgc);
	FILE *fp = fopen(fname, "w");
	char out[2* (dim * dim) + 1];
	print_grid(out);
	fputs(out, fp);
	fclose(fp);
	fgc++;
}

void print_grid(char *out) {
	int i, j, c=0;
	for(i=0; i<dim; i++) {
		for(j=0; j<dim; j++) {
			out[c] = grid[i][j].s + '0';
			c++;
			out[c] = '\t';
			c++;
		}
		/* out[c] = '\n';
		c++; */
	}
	out[c] = '\0';
}

