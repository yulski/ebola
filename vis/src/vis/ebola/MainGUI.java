package vis.ebola;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.awt.event.*;
import java.io.*;

public class MainGUI extends JFrame {
	
	private java.util.Timer timer;
	private JPanel main;
	private Grid[] panels;
	private JButton next, previous, start, stop;
	private int counter;
	private JLabel label;
	private CardLayout layout;

	public MainGUI(Grid[] panels) {
		this.panels = panels;
		this.layout = new CardLayout();
		this.main = new JPanel(this.layout);
		for(int i=0; i<panels.length; i++) {
			this.panels[i].setPreferredSize(new Dimension(1000, 1000));
			this.main.add(this.panels[i]);
		}

		this.label = new JLabel("currently showing: " + this.panels[0].getName());

		this.previous = new JButton("previous");
		this.previous.addActionListener(this::previousAction);
		this.next = new JButton("next");
		this.next.addActionListener(this::nextAction);
		this.start = new JButton("start");
		this.start.addActionListener(this::startAction);
		this.stop = new JButton("stop");
		this.stop.addActionListener(this::stopAction);

		JPanel buttonPanel = new JPanel();
		buttonPanel.add(previous);
		buttonPanel.add(next);
		buttonPanel.add(start);
		buttonPanel.add(stop);
		
		this.add(this.label, BorderLayout.NORTH);
		this.add(buttonPanel, BorderLayout.SOUTH);
		this.add(new JScrollPane(this.main));

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	private void nextAction(ActionEvent e) {
		this.layout.next(this.main);
		this.counter++;
		if(this.counter > this.panels.length - 1) {
			this.counter = 0;
		}
		this.label.setText("currently showing: " + this.panels[this.counter].getName());
	}

	private void previousAction(ActionEvent e) {
		this.layout.previous(this.main);
		this.counter--;
		if(this.counter < 0) {
			this.counter = this.panels.length - 1;
		}
		this.label.setText("currently showing: " + this.panels[this.counter].getName());
	}

	private void startAction(ActionEvent e) {
		this.next.setVisible(false);
		this.previous.setVisible(false);
		this.timer = new java.util.Timer();
		this.timer.schedule(new TimerTask() {
			public void run() {
				MainGUI.this.next.doClick();
			}
		}, 0, 500);
	}

	private void stopAction(ActionEvent e) {
		this.timer.cancel();
		this.next.setVisible(true);
		this.previous.setVisible(true);
	}

}

