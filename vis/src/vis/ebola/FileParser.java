package vis.ebola;

import java.util.*;
import java.io.*;
import java.nio.file.*;

public class FileParser {

	public static Integer[] parseFile(File dataFile) {
		try {
			List<String> list = Files.readAllLines(dataFile.toPath());
			String[] sep = list.get(0).split("\t", -1);
			Integer[] arr = Arrays.stream(sep)
				.filter(str -> str.length() > 0)
				.map(Integer::parseInt)
				.toArray(Integer[]::new);
			return arr;
		} catch(IOException e) {
			e.printStackTrace();
			return new Integer[1];
		}
	}

}

