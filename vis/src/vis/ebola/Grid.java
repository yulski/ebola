package vis.ebola;

import javax.swing.*;
import java.awt.*;

public class Grid extends JPanel {

	private static final int HEALTHY = 0;
	private static final int INFECTED = 1;
	private static final int CONTAGIOUS = 2;
	private static final int DEAD = 3;

	private Integer[] grid;
	private int dim;

	public Grid(Integer[] grid, int dim) {
		this.grid = grid;
		this.dim = dim;
		System.out.println("grid width: " + grid.length); // TODO: remove
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		int x = 0;
		int y = 0;
		int val;

		int w = 1000;
		int h = 1000;

		if(dim < 1000) {
			w = 1000 / dim;
			h = 1000 / dim;
		}

		for(int i=0; i<dim; i++) {
			for(int j=0; j<dim; j++) {
				val = grid[i * dim + j];
				switch(val) {
					case HEALTHY:
						g.setColor(Color.GREEN);
						break;
					case INFECTED:
						g.setColor(Color.YELLOW);
						break;
					case CONTAGIOUS:
						g.setColor(Color.RED);
						break;
					case DEAD:
						g.setColor(Color.BLACK);
						break;
				}
				g.fillRect(x, y, w, h);
				x += w;
			}
			x = 0;
			y += h;
		}
	}

}

