package vis.ebola;

import java.io.*;
import java.awt.*;
import javax.swing.*;

public class VisRun {

	public static void main(String[] args) {
		if(args.length < 2 || args[0].equals("-help")) {
			printInstructions();
		}
		int fileCount = Integer.parseInt(args[0]);
		int dim = Integer.parseInt(args[1]);
		String dir = "";
		if(args.length > 2) {
			dir = args[2];
		}
		File[] files = new File[fileCount];
		Grid[] panels = new Grid[fileCount];
		for(int i=0; i<files.length; i++) {
			files[i] = new File(dir + "/ebola-grid-" + i + ".dat");
			panels[i] = new Grid(FileParser.parseFile(files[i]), dim);
			panels[i].setName(files[i].getName());
		}
		SwingUtilities.invokeLater(() -> {
			new MainGUI(panels);
		});
	}

	public static void printInstructions() {
		System.out.println("2 required argumesnts and 1 optional");
		System.out.println("args[0] = file count (required)");
		System.out.println("args[1] = dim (required)");
		System.out.println("args[2] = dir (optional)");
		System.out.println("\n");
	}

}

